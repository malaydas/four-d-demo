import React, { useState, useEffect } from 'react'
import './App.css'
import axios from 'axios'
import { Button, Modal, Card, Container, Row, Col } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

const App = () => {

  const [me, setMe] = useState('')
  const [transactionDetails, setTransactionDetails] = useState('')
  const [projectStateInfo, setProjectStateInfo] = useState({})
  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    axios
      .get('http://localhost:10009/api/iou/me', {
        headers: {
          "Access-Control-Allow-Origin": "*"
        },
        responseType: 'json',
      })
      .then(response => {
        setMe(response.data.me)
        //console.log(me)
      })
  }, [])

  useEffect(() => {
    axios
      .get('http://localhost:10009/api/iou/project-state', {
        headers: {
          "Access-Control-Allow-Origin": "*"
        },
        responseType: 'json',
      })
      .then(response => {
        setProjectStateInfo({})
        const entireData = response.data
        const totalPaid = entireData[entireData.length - 1].state.data.totalPaid
        const paid = entireData[entireData.length - 1].state.data.paid
        const workCompleted = entireData[entireData.length - 1].state.data.workCompletion
        const allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
        setProjectStateInfo({
          'totalPaid': totalPaid,
          'paid': paid,
          'workCompleted': workCompleted,
          'allocatedAmount': allocatedAmount
        })
      })
  }, [setProjectStateInfo, transactionDetails])

  const showAllocateTransaction = () => {
    axios
      .put('http://localhost:10009/api/iou/allocate-project?amount=1000&currency=EUR&party=O=CustodyService,L=Paris,C=FR', {
        headers: {
          "Access-Control-Allow-Origin": "*"
        },
        responseType: 'json',
      })
      .then(response => {
        setTransactionDetails(response.data)
        handleShow()
      })
  }

  const showFullCompletionTransaction = () => {
    axios
      .put('http://localhost:10009/api/iou/complete-work?percent=100&currency=EUR&party=O=Builder,L=New%20York,C=US', {
        headers: {
          "Access-Control-Allow-Origin": "*"
        },
        responseType: 'json',
      })
      .then(response => {
        setTransactionDetails(response.data)
        handleShow()
      })
  }

  const showHalfCompletionTransaction = () => {
    axios
      .put('http://localhost:10009/api/iou/complete-work?percent=50&currency=EUR&party=O=Builder,L=New%20York,C=US', {
        headers: {
          "Access-Control-Allow-Origin": "*"
        },
        responseType: 'json',
      })
      .then(response => {
        setTransactionDetails(response.data)
        handleShow()
      })
  }

  const reset = () => {
    axios
      .put('http://localhost:10009/api/iou/reset', {
        headers: {
          "Access-Control-Allow-Origin": "*"
        },
        responseType: 'json',
      })
      .then(response => {
        setProjectStateInfo({})
        const entireData = response.data
        const totalPaid = entireData[entireData.length - 1].state.data.totalPaid
        const paid = entireData[entireData.length - 1].state.data.paid
        const workCompleted = entireData[entireData.length - 1].state.data.workCompletion
        const allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
        setProjectStateInfo({
          'totalPaid': totalPaid,
          'paid': paid,
          'workCompleted': workCompleted,
          'allocatedAmount': allocatedAmount
        })
      })
  }

  return (
    <Container>
      <Row>
        <Col>
          <h1 style={{ marginBottom: '2rem' }}>{me}</h1>
          {(me === 'Investor') ? <Button style={{ marginRight: '1rem' }} variant="primary" onClick={showAllocateTransaction}>Allocate 1000 EUR</Button> : null}
          {(me === 'Builder') ? <Button style={{ marginRight: '1rem' }} variant="primary" onClick={showHalfCompletionTransaction}>Confirm 50% complete</Button> : null}
          {(me === 'Builder') ? <Button style={{ marginRight: '1rem' }} variant="primary" onClick={showFullCompletionTransaction}>Confirm 100% complete</Button> : null}
          <Button variant="primary" onClick={reset}>Reset</Button>
        </Col>
        <Col>
        </Col>
      </Row>

      <Row>
        <Modal size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Transaction Successful</Modal.Title>
          </Modal.Header>
          <Modal.Body>{transactionDetails}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>

        <Col>
          <Card style={{ width: '28rem', marginTop: '3rem' }}>
            <Card.Body>
              <Card.Title>Project State</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">{me}'s view</Card.Subtitle>
              <Card.Text>
                Work completed: {projectStateInfo.workCompleted} percent
              </Card.Text>
              <Card.Text>
                Paid: {projectStateInfo.paid}
              </Card.Text>
              <Card.Text>
                Total paid: {projectStateInfo.totalPaid === null ? 0 : projectStateInfo.totalPaid}
              </Card.Text>
              <Card.Text>
                Allocated amount: {projectStateInfo.allocatedAmount}
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default App
